package io.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 
public class JdbcCon {
    public static void main(String[] argv) {
 
        System.out.println("-------- MySQL JDBC Connection Demo ------------");
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
        } 
        catch (ClassNotFoundException e) {
            System.out.println("MySQL JDBC Driver not found !!");
            return;
        }
        System.out.println("MySQL JDBC Driver Registered!");
        Connection connection = null;
        try {
            connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/firstdb", "root", "root");

            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/firstdb","root", "root");

            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM student");
            System.out.println("id  name address");

            while (rs.next()) {
               int id = rs.getInt("id");
               String name = rs.getString("name");
               String address = rs.getString("address");
               System.out.println(id+"   "+name+"  "+address);
            }

        } catch (SQLException e) {
            System.out.println("Connection Failed!");
            return;
        } finally {
            try
            {
                if(connection != null)
                    connection.close();
                System.out.println("Connection closed");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

